package mino.ermal.engine3d;

import mino.ermal.engine3d.renderer.D3Renderer;
import mino.ermal.engine3d.renderer.zbuffer.ZBufferRenderer;
import mino.ermal.engine3d.interfaces.Transformable;
import mino.ermal.engine3d.matrix.RotationMatrix3D;
import mino.ermal.engine3d.object3d.Object3D;
import mino.ermal.engine3d.object3d.World;
import mino.ermal.engine3d.primitives.Vertex;
import mino.ermal.engine3d.shapes.*;

import javax.swing.*;
import java.awt.*;
import java.awt.image.ImageObserver;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import mino.ermal.engine3d.matrix.Matrix3D;
import mino.ermal.engine3d.object3d.Element3D;
import mino.ermal.engine3d.renderer.zbuffer.ZelementRenderer;

/**
 * User: mino.ermal(AT)gmail.com Date: 9-dic-2006 Time: 18.25.31
 *
 * This panel is where the drawing takes place. It implements Transformable and
 * D3Renderer to make it straight forward for whatever object uses this panel to
 * execute transformations and rendering. It can be integrated to whatever user
 * interface we want.
 *
 */
public class Engine3DRenderPanel extends JPanel implements Transformable, D3Renderer {

    //map containing 3d objects
    private final Map objectList = new LinkedHashMap();
    private World world; //world object to put 3d objects here before rendering
    private Element3D ship; //world object to put 3d objects here before rendering
    private Element3D torus; //world object to put 3d objects here before rendering
    private Element3D asteroid1; //world object to put 3d objects here before rendering
    private Element3D asteroid2; //world object to put 3d objects here before rendering
    private Element3D asteroid3; //world object to put 3d objects here before rendering
    private D3Renderer renderer; //the renderer

    /**
     * Creates a new 3DRenderer Panel
     */
    public Engine3DRenderPanel() {
        super();
        init();
    }

    /**
     * Initialize 3D objects and put them in a list.
     */
    private void init() {
        Object3D object3d;
       // renderer=new ZBufferRenderer(); //create a new Z-buffer renderer
        renderer = new ZelementRenderer(); //create a new Z-buffer renderer
        //set the wireframe color to white
        renderer.setWireframeColor(255, 255, 255);
        world = new World(); //create a new world object

        object3d = new CoordinateAxis(1);
        world.add(object3d);

        createScene();
        initThreads();
    }

    /**
     * Superclass method override. This method creates an offscreen image and
     * renders offscreen first before effectively drawing the result in the
     * panel graphics for speed and image flickering problem
     *
     * @see javax.swing.JPanel#paint(java.awt.Graphics)
     * @param gr Graphics object used to paint
     */
    public void paint(Graphics gr) {

        //create an offscreen image of the size of this panel
        Image view = this.createImage(this.getWidth(), this.getHeight());

        //obtain image graphics
        Graphics igr = view.getGraphics();

        //black background in simple 3D rendering looks cool :D
        igr.setColor(Color.DARK_GRAY);
        //paint all image with black as background
        igr.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());

        //render in this image
        renderer.render(world, view, this);

        //draw the image on screen
        gr.drawImage(view, 0, 0, this);
    }

    /**
     * It is invoked to select and change the current rendered object
     *
     * @param objName the key of the object contained in the map
     */
    public void select(String objName) {

        //remove previous objects on the world
        world.clear();

        //add the selected object to the world
        world.add((Object3D) objectList.get(objName));

        //refresh the painting
        //repaint();
    }

    /**
     * Gets the iterator object of the key set of the map containing the
     * objects. This is useful to the control panel that will be using this one
     * to know the objects contained herein
     *
     * @return the key set iterator object
     */
    public Iterator getShapeList() {
        return objectList.keySet().iterator();
    }

    /**
     * refresh rendering on resize when no changes occur to the objects
     */
    public void render() {
        //repaint();
    }


    /*
    the following methods are the implementations of the interfaces
    Transformable and D3Renderer
     */
    public void scale(double factor) {
        world.scale(factor);
        //repaint();
    }

    public void scale(Vertex vector) {
        world.scale(vector);
        //repaint();
    }

    public void rotate(RotationMatrix3D rotationMatrix) {
        //world.rotate(rotationMatrix);
        ship.rotate(rotationMatrix);
        //repaint();
    }

    public void rotateWorld(RotationMatrix3D rotationMatrix) {
        world.rotate(rotationMatrix);
        //ship.rotate(rotationMatrix);
        //repaint();
    }

    public void translate(Vertex vector) {
        world.translate(vector);
        //repaint();
    }

    public void setColor(int r, int g, int b) {
        //not needed
    }

    public void render(World world, Image view, ImageObserver observer) {
        //not needed
    }

    public void setWireframeColor(int r, int g, int b) {
        renderer.setWireframeColor(r, g, b);
    }

    public void setRenderAxis(boolean renderAxis) {
        renderer.setRenderAxis(renderAxis);
    }

    public boolean getRenderAxis() {
        return renderer.getRenderAxis();
    }

    public void setWireframed(boolean wireframed) {
        renderer.setWireframed(wireframed);
    }

    public boolean getWireframed() {
        return renderer.getWireframed();
    }

    public void setFilled(boolean filled) {
        renderer.setFilled(filled);
    }

    public boolean getFilled() {
        return renderer.getFilled();
    }

    private void initThreads() {
 
        

    }

    private void createScene() {
        ship = new SpaceShip();
        ship.scale(10D);
        ship.setOrigin(new Vertex(50, 50,50));
        ship.rotate(new RotationMatrix3D(0,Math.PI/2,0));
        ship.setColor(0, 255, 255);
        ship.addCoordinateAxis(40D);
        objectList.put("ship", ship);
        world.add(ship);
        
        torus = new Torus(15, 30, 5, 20);
        torus.setOrigin(new Vertex(0, 0, 0));
        torus.scale(5);
        torus.addCoordinateAxis(50);
        objectList.put("Torus", torus);
       // world.add(torus);
         
        asteroid1 = new Asteroid();
        asteroid1.setOrigin(new Vertex(0, 0, 0));
        asteroid1.scale(10);
        asteroid1.addCoordinateAxis(50);
        objectList.put("asteroid1", asteroid1);
        world.add(asteroid1);
    }
}
