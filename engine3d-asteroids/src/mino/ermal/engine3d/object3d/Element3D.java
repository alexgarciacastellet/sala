/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mino.ermal.engine3d.object3d;

import mino.ermal.engine3d.interfaces.Transformable;
import mino.ermal.engine3d.matrix.RotationMatrix3D;
import mino.ermal.engine3d.primitives.Vertex;
import mino.ermal.engine3d.shapes.CoordinateAxis;

/**
 *
 * @author alex
 */
public class Element3D extends Object3D {

    Vertex origin = new Vertex(0, 0, 0);
    Vertex rotation = new Vertex(0, 0, 0);
    Vertex rotation_speed = new Vertex(0, 0, 0);
    Vertex speed = new Vertex(0, 0, 0);
    Vertex acceleration = new Vertex(0, 0, 0);

    public CoordinateAxis getBase() {
        return base;
    }
    CoordinateAxis base = new CoordinateAxis(1);

    public Element3D() {
        super();
    }

    public Vertex getOrigin() {
        return origin;
    }

    public void setOrigin(Vertex origin) {
        this.origin = origin;
    }

    public Vertex getRotation() {
        return rotation;
    }

    public void setRotacions(Vertex rotacions) {
        this.rotation = rotacions;
    }

    public void rotate(RotationMatrix3D rotationMatrix) {
        base.rotate(rotationMatrix);
    }

    @Override
    public void translate(Vertex vector) {
        int w = 800;
        int h = 600;
        int d = 800;
        this.origin.x += vector.x; //To change body of generated methods, choose Tools | Templates.
        this.origin.y += vector.y; //To change body of generated methods, choose Tools | Templates.
        this.origin.z += vector.z; //To change body of generated methods, choose Tools | Templates.

        if (this.origin.x > w) {
            this.origin.x = -w;
        }
        if (this.origin.x < -w) {
            this.origin.x = w;
        }
        if (this.origin.y > h) {
            this.origin.y = -h;
        }
        if (this.origin.y < -h) {
            this.origin.y = h;
        }
        if (this.origin.z > d) {
            this.origin.z = -d;
        }
        if (this.origin.z < -d) {
            this.origin.z = d;
        }
    }
}
