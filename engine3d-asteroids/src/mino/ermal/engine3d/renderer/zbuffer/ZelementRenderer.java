package mino.ermal.engine3d.renderer.zbuffer;

import mino.ermal.engine3d.primitives.Segment;
import mino.ermal.engine3d.primitives.Triangle;
import mino.ermal.engine3d.primitives.Vertex;
import mino.ermal.engine3d.util.VectorUtil;
import mino.ermal.engine3d.object3d.SegmentBasedObject3D;
import mino.ermal.engine3d.object3d.World;
import mino.ermal.engine3d.object3d.Object3D;
import mino.ermal.engine3d.renderer.D3Renderer;

import java.awt.*;
import java.awt.image.ImageObserver;
import mino.ermal.engine3d.object3d.Element3D;
import mino.ermal.engine3d.shapes.CoordinateAxis;

/**
 * User: mino.ermal(AT)gmail.com Date: Sep 30, 2007 Time: 4:41:48 PM
 *
 * Z-Buffer based renderer
 *
 */
public class ZelementRenderer implements D3Renderer {

    //perspective division constant
    private static final double D = 400;

    private int wireframeRed = 255; //red wireframe color component
    private int wireframeGreen = 0; //green wireframe color component
    private int wireframeBlue = 0;  //blue wireframe color component

    private boolean renderAxis = false; //render axis flag
    private boolean wireframed = false; //render wireframe flag
    private boolean filled = true; //render filled flag

    /**
     * Renders segment based objects
     *
     * @param object3d object to be rendered
     * @param view off screen image
     * @param observer image observer object
     * @param zbuffer z depth buffer
     */
    private void renderSegmentBased(Object3D object3d, Image view, ImageObserver observer, int[][] zbuffer) {

        Graphics gr = view.getGraphics(); //obtain graphic object from off screen image

        int w = view.getWidth(observer); //obtain image width
        int h = view.getHeight(observer); //obtain image height

        for (int i = 0; object3d != null && i < object3d.transformables.size(); i++) {
            Segment segment = (Segment) object3d.transformables.get(i);

            //sets the color of the graphic object used to draw
            gr.setColor(new Color(segment.r, segment.g, segment.b));

            //calculate perspective transformation for all vertexes
            //and for x,y and z coordinates
            int x1, x2;
            x1 = (int) (segment.start.x / (1 + segment.start.z / D)) + w / 2;
            x2 = (int) (segment.end.x / (1 + segment.end.z / D)) + w / 2;

            int y1, y2;
            y1 = (int) (segment.start.y / (1 + segment.start.z / D)) + h / 2;
            y2 = (int) (segment.end.y / (1 + segment.end.z / D)) + h / 2;

            int z1, z2;
            z1 = (int) segment.start.z;
            z2 = (int) segment.end.z;

            //draw line
            ZbufferUtil.drawLine(gr, x1, y1, z1, x2, y2, z2, zbuffer);

        }

    }

    /**
     * Renders triangle based objects
     *
     * @param object3d object to be rendered
     * @param view off screen image
     * @param observer image observer object
     * @param zbuffer z depth buffer
     */
    private void renderTriangleBased(CoordinateAxis base, Element3D object3d, Image view, ImageObserver observer, int[][] zbuffer) {
        Graphics gr = view.getGraphics(); //obtain graphic object from offscreen image
        if (false) {
            return;
        }
        int w = view.getWidth(observer); //obtain image width
        int h = view.getHeight(observer); //obtain image height

        Vertex i = ((Segment) base.transformables.get(0)).end;
        Vertex j = ((Segment) base.transformables.get(1)).end;
        Vertex k = ((Segment) base.transformables.get(2)).end;
/*
        String st = String.valueOf(i.x) + " ** " + String.valueOf(i.y) + " ** " + String.valueOf(i.z);
        String st2 = String.valueOf(j.x) + " ** " + String.valueOf(j.y) + " ** " + String.valueOf(j.z);
        String st3 = String.valueOf(k.x) + " ** " + String.valueOf(k.y) + " ** " + String.valueOf(k.z);

        System.out.println(st);
        System.out.println(st2);
        System.out.println(st3);
        System.out.println("-----------------------");
*/
        Vertex origin = object3d.getOrigin();
        // Vertex origin = new Vertex(0,0,0);
        
        
        for (int n = 0; n < object3d.transformables.size(); n++) {
           // while (!object3d instanceof Triangle) object3d = object3d.transformables.get(n);
                
            Triangle triangle = (Triangle) object3d.transformables.get(n);
            
            Triangle tri = new Triangle(Vertex.clon(triangle.v1), Vertex.clon(triangle.v2), Vertex.clon(triangle.v3));

            int[] x = new int[3];
            int[] y = new int[3];
            int[] z = new int[3];
            

       
        CoordinateAxis ebase = object3d.getBase();
        
        Vertex ei = ((Segment) ebase.transformables.get(0)).end;
        Vertex ej = ((Segment) ebase.transformables.get(1)).end;
        Vertex ek = ((Segment) ebase.transformables.get(2)).end;
            tri.v1 = Vertex.vertex2Axes(tri.v1, ei, ej, ek);
            tri.v2 = Vertex.vertex2Axes(tri.v2, ei, ej, ek);
            tri.v3 = Vertex.vertex2Axes(tri.v3, ei, ej, ek);



            Vertex v1 = Vertex.vertex2Axes(origin, tri.v1, i, j, k);
            Vertex v2 = Vertex.vertex2Axes(origin, tri.v2, i, j, k);
            Vertex v3 = Vertex.vertex2Axes(origin, tri.v3, i, j, k);

            if (v1.z==-D) v1.z+=0.1;
            if (v2.z==-D) v1.z+=0.1;
            if (v3.z==-D) v1.z+=0.1;

            x[0] = (int) (v1.x/(1+v1.z/D)) + w / 2;
            y[0] = (int) (v1.y/(1+v1.z/D)) + h / 2;
            z[0] = (int) (v1.z);

            x[1] = (int) (v2.x/(1+v2.z/D)) + w / 2;
            y[1] = (int) (v2.y/(1+v2.z/D)) + h / 2;
            z[1] = (int) (v2.z);

            x[2] = (int) (v3.x/(1+v3.z/D)) + w / 2;
            y[2] = (int) (v3.y/(1+v3.z/D)) + h / 2;
            z[2] = (int) (v3.z);

                        Vertex normale= VectorUtil.calculateNormal(new Triangle(v1,v2,v3));

            double cos= VectorUtil.calculateAngleCosBetweenVectors(normale,new Vertex(Math.cos(Math.PI/2),Math.cos(Math.PI/2)/2,Math.cos(Math.PI/2)));
			int r,g,b;
			cos=Math.abs(cos);
            r=(int)(cos*tri.r);
            g=(int)(cos*tri.g);
            b=(int)(cos*tri.b);

            //set graphic color
            gr.setColor(new Color(r,g,b));

            //if filled flag is true render filled triangles
            if(filled) ZbufferUtil.fillTriangle(gr,x,y,z,zbuffer);
            /*
            gr.setColor(new Color(wireframeRed, wireframeGreen, wireframeBlue));
            //gr.drawOval(x[1], y[1], 3, 3);
                ZbufferUtil.drawLine(gr,x[0], y[0], z[0], x[1], y[1], z[1], zbuffer );
                ZbufferUtil.drawLine(gr,x[1], y[1], z[1], x[2], y[2], z[2], zbuffer );
                ZbufferUtil.drawLine(gr,x[2], y[2], z[2], x[0], y[0], z[0], zbuffer );
            */
        }

    }

    /**
     *
     * @param world world object containing the set of 3d objects
     * @param view offscreen image to render the world in
     * @param observer image observer object necessary to get width and height
     * of the
     */
    public void render(World world, Image view, ImageObserver observer) {

        int w = view.getWidth(observer); //obtain image width
        int h = view.getHeight(observer); //obtain image height

        int[][] zbuffer = new int[w][h];
        //initialize zbuffer with tha max depth value
        for (int k = 0; k < w; k++) {
            for (int n = 0; n < h; n++) {
                zbuffer[k][n] = 65534;
            }
        }

        Object3D obj = (Object3D) world.transformables.get(0);
        renderSegmentBased(obj, view, observer, zbuffer);

        for (int i = 1; i < world.transformables.size(); i++) {
            Element3D object3d = (Element3D) world.transformables.get(i);
            renderTriangleBased((CoordinateAxis) world.transformables.get(0), object3d, view, observer, zbuffer);
 
        }
    }

    /**
     * @see mino.ermal.engine3d.renderer.D3Renderer#setWireframeColor(int, int,
     * int)
     * @param r red component
     * @param g green component
     * @param b blue component
     */
    public void setWireframeColor(int r, int g, int b) {
        if (r >= 0 && r < 256) {
            this.wireframeRed = r;
        }
        if (g >= 0 && g < 256) {
            this.wireframeGreen = g;
        }
        if (b >= 0 && b < 256) {
            this.wireframeBlue = b;
        }
    }

    /**
     * @see mino.ermal.engine3d.renderer.D3Renderer#setRenderAxis(boolean)
     * @param renderAxis render axis flag
     */
    public void setRenderAxis(boolean renderAxis) {
        this.renderAxis = renderAxis;
    }

    /**
     * @return render axis flag
     */
    public boolean getRenderAxis() {
        return this.renderAxis;
    }

    /**
     * @see mino.ermal.engine3d.renderer.D3Renderer#setWireframed(boolean)
     * @param wireframed render wireframe flag
     */
    public void setWireframed(boolean wireframed) {
        this.wireframed = wireframed;
    }

    /**
     * @return render wireframe flag
     */
    public boolean getWireframed() {
        return this.wireframed;
    }

    /**
     * @see mino.ermal.engine3d.renderer.D3Renderer#setFilled(boolean)
     * @param filled render filled flag
     */
    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    /**
     * @return render filled flag
     */
    public boolean getFilled() {
        return this.filled;
    }
}
