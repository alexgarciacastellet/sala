package mino.ermal.engine3d.primitives;

import mino.ermal.engine3d.interfaces.Transformable;
import mino.ermal.engine3d.matrix.RotationMatrix3D;

/**
 * User: mino.ermal(AT)gmail.com Date: 9-dic-2006 Time: 18.51.45
 *
 * This class represents a vertexes, points and vectors a simple object with 4
 * fields out of which only 3 are used for the moment. The Transformable
 * interface methods are effectively implemented only here. Other Transformable
 * children only delegate dhe method call.
 */
public class Vertex implements Transformable {

    public double x;
    public double y;
    public double z;
    public double w;

    /**
     * Creates a 0 vector
     */
    public Vertex() {
        x = 0;
        y = 0;
        z = 0;
        w = 1;
    }

    /**
     * Creates a vector with the parameter values
     *
     * @param x X coordinate
     * @param y Y coordinate
     * @param z Z coordinate
     */
    public Vertex(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = 1;
    }

    /**
     * Creates a vector with the parameter values
     *
     * @param x X coordinate
     * @param y Y coordinate
     * @param z Z coordinate
     * @param w W coordinate (not actually used as a coordinate)
     */
    public Vertex(double x, double y, double z, double w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    /**
     * @see mino.ermal.engine3d.interfaces.Transformable#scale(double)
     * @param factor scale factor
     */
    public void scale(double factor) {
        this.x *= factor;
        this.y *= factor;
        this.z *= factor;
    }

    /**
     * @see
     * mino.ermal.engine3d.interfaces.Transformable#scale(mino.ermal.engine3d.primitives.Vertex)
     * @param vector Vertex value
     */
    public void scale(Vertex vector) {
        this.x *= vector.x;
        this.y *= vector.y;
        this.z *= vector.z;
    }

    /**
     * @see
     * mino.ermal.engine3d.interfaces.Transformable#rotate(mino.ermal.engine3d.matrix.RotationMatrix3D)
     * @param rotationMatrix rotation matrix
     */
    public void rotate(RotationMatrix3D rotationMatrix) {
        rotationMatrix.multiply(this);
    }

    /**
     * @see
     * mino.ermal.engine3d.interfaces.Transformable#translate(mino.ermal.engine3d.primitives.Vertex)
     * @param vector translation vector
     */
    public void translate(Vertex vector) {
        this.x += vector.x;
        this.y += vector.y;
        this.z += vector.z;
    }

    /**
     * @see mino.ermal.engine3d.interfaces.Transformable#setColor(int, int, int)
     * @param r red (0..255)
     * @param g green (0..255)
     * @param b blue (0..255)
     */
    public void setColor(int r, int g, int b) {
        //not implemented for the moment
        //will be useful when implementing phong shading
    }

    /**
     * Projects the v Vertex in an i,j,k axes system
     * @param v vertex to project
     * @param i unitary vertex for z axis
     * @param j unitary vertex for z axis
     * @param k unitary vertex for z axis
     */
    public static Vertex vertex2Axes10(Vertex v, Vertex i, Vertex j, Vertex k) {
        int x, y, z;

        x = (int) (((v.x) * i.x + (v.y) * j.x + (v.z) * k.x));
        y = (int) (((v.x) * i.y + (v.y) * j.y + (v.z) * k.y));
        z = (int) ((v.x) * i.z + (v.y) * j.z + (v.z) * k.z);

        return new Vertex(x, y, z);
    }
    
public static Vertex clon(Vertex v){
    return new Vertex(v.x,v.y,v.z);
} 

        public static Vertex vertex2Axes(Vertex v, Vertex i, Vertex j, Vertex k) {
            return Vertex.vertex2Axes(new Vertex(0,0,0),v,i,j,k);
        }
 
    public static Vertex vertex2Axes( Vertex origin, Vertex v, Vertex i, Vertex j, Vertex k) {
        int x, y, z;

        int inc=25;
        x = (int) (((v.x+origin.x) * i.x + (v.y+origin.y) * j.x + (v.z+origin.z) * k.x));
        y = (int) (((v.x+origin.x) * i.y + (v.y+origin.y) * j.y + (v.z+origin.z) * k.y));
        z = (int) ((v.x+origin.x) * i.z + (v.y+origin.y) * j.z + (v.z+origin.z) * k.z);

        return new Vertex(x, y, z);
    }

    public static Vertex vertexSum(Vertex v1, Vertex v2) {
        double x, y, z;
        x = v1.x + v2.x;
        y = v1.y + v2.y;
        z = v1.z + v2.z;

        return new Vertex(x, y, z);
    }

}
