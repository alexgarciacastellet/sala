package mino.ermal.engine3d.shapes;

import mino.ermal.engine3d.object3d.SegmentBasedObject3D;
import mino.ermal.engine3d.object3d.TriangleBasedObject3D;

import mino.ermal.engine3d.primitives.Triangle;
import mino.ermal.engine3d.primitives.Vertex;

/**
 * User: mino.ermal(AT)gmail.com
 * Date: 9-dic-2006
 * Time: 18.49.37
 *
 * This class creates a Cube (only segment based,
 * the triangle based has can be implemented too)
 *
 */
public class SpaceShip extends TriangleBasedObject3D   {

    /**
     * Creates a new wireframe cube
     */
    public SpaceShip(){
		super();
//	
                //BASE
              //  transformables.add(new Triangle(new Vertex(2,1,1),new Vertex(-2,1,1),new Vertex(5,-2,5)));
               
                
                //transformables.add(new Triangle(new Vertex(2,0,0),new Vertex(-2,0,0),new Vertex(0,-2,0)));
		//transformables.add(new Triangle(new Vertex(2,0,0),new Vertex(-2,0,0),new Vertex(0,2,0)));
		transformables.add(new Triangle(new Vertex(2,0,0),new Vertex(0,2,0),new Vertex(0,0,-4)));
		transformables.add(new Triangle(new Vertex(-2,0,0),new Vertex(0,2,0),new Vertex(0,0,-4)));
		transformables.add(new Triangle(new Vertex(2,0,0),new Vertex(0,-2,0),new Vertex(0,0,-4)));
		transformables.add(new Triangle(new Vertex(-2,0,0),new Vertex(0,-2,0),new Vertex(0,0,-4)));
                
                
                
	}
	

}
