package mino.ermal.engine3d.shapes;

import mino.ermal.engine3d.object3d.SegmentBasedObject3D;
import mino.ermal.engine3d.object3d.TriangleBasedObject3D;

import mino.ermal.engine3d.primitives.Triangle;
import mino.ermal.engine3d.primitives.Vertex;

/**
 * User: mino.ermal(AT)gmail.com
 * Date: 9-dic-2006
 * Time: 16.49.37
 *
 * This class creates a Cube (only segment based,
 * the triangle based has can be implemented too)
 *
 */
public class Asteroid extends TriangleBasedObject3D   {

    /**
     * Creates a new wireframe cube
     */
    public Asteroid(){
		super();
//	
                //BASE
              //  transformables.add(new Triangle(new Vertex(2,1,1),new Vertex(-2,1,1),new Vertex(5,-2,5)));
               
                
		transformables.add(new Triangle(new Vertex(22,20,0),new Vertex(20,22,0),new Vertex(20,20,-2)));
		transformables.add(new Triangle(new Vertex(18,20,0),new Vertex(20,22,0),new Vertex(20,20,-2)));
		transformables.add(new Triangle(new Vertex(22,20,0),new Vertex(20,18,0),new Vertex(20,20,-2)));
		transformables.add(new Triangle(new Vertex(18,20,0),new Vertex(20,18,0),new Vertex(20,20,-2)));
		transformables.add(new Triangle(new Vertex(22,20,0),new Vertex(20,22,0),new Vertex(20,20,2)));
		transformables.add(new Triangle(new Vertex(18,20,0),new Vertex(20,22,0),new Vertex(20,20,2)));
		transformables.add(new Triangle(new Vertex(22,20,0),new Vertex(20,18,0),new Vertex(20,20,2)));
		transformables.add(new Triangle(new Vertex(18,20,0),new Vertex(20,18,0),new Vertex(20,20,2)));
	}
/*	
    public void animation(){
        this.rotate();
    }
*/
}
